import './App.css'
import NavBar from "./components/layout/NavBar";
import Home from "./pages/Home.jsx";
import Login from "./pages/auth/Login";
import {Routes, Route, Link} from "react-router-dom";
import Dashboard from "./pages/Dashboard.jsx";
import RequireAuth from "./components/RequireAuth.jsx";
import GuestUser from "./components/GuestUser.jsx";

export default function App() {
    const routes = [
        {path: '/', name: 'Home', Component: <Home/>, exact: true},
        {path: '/login', name: 'Login', Component:<GuestUser> <Login/></GuestUser>, exact: false},
        {path: '/dashboard', name: 'Dashboard', Component: <RequireAuth> <Dashboard/></RequireAuth>, exact: false},

    ];
    return (
        <>
            <NavBar/>
            <Routes>
                {
                    routes.map(({path, Component, exact}) => (
                        <Route key={path} path={path} exact={exact} element={Component}></Route>
                    ))
                }
            </Routes>

        </>
    )
}


