import {createContext, useContext, useMemo} from "react";
import useLocalStorage from "./useLocalStorage.js";
const AuthContext = createContext();

export function AuthProvider({children}) {
    const [user, setUser] = useLocalStorage("user", null);
    const login = async (data) => {
        setUser(data);
    };
    const logout = () => {
        setUser(null);
    };
    const auth = useMemo(
        () => ({
            user,
            login,
            logout
        }),
        [user]
    );
    return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
    return useContext(AuthContext);

};
