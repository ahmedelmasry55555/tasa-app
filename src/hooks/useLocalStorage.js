import { useEffect, useState } from 'react';

export default function useLocalStorage(key, init = '') {
  // const [value, setValue] = useState(null);
  const [value, setValue] = useState(() => {
    const item = localStorage.getItem(key);
    return item ? JSON.parse(item) : init;

  });

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value)??'');
  }, [value]);

  return [value, setValue];
}