import {useState} from "react";

export default function useServerSideValidationErrors(responseErrors) {
    const [errors, setErrors] = useState(responseErrors);

    function has(field) {
        return errors.hasOwnProperty(field);
    }


    function any() {
        return Object.keys(errors).length > 0;
    }


    function get(field) {
        if (has(field)) {
            return errors[field][0];
        }
    }

    return {
        errors, setErrors, get, any, has
    }
}