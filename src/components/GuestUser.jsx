import {useAuth} from "../hooks/useAuth.jsx";
import {Navigate} from "react-router-dom";

export default function GuestUser({children}) {
    const {user} = useAuth();
    return user ? <Navigate to="/dashboard" replace/> : children;

}