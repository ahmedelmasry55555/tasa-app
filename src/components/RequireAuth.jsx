import {useAuth} from "../hooks/useAuth.jsx";
import {Navigate,useLocation} from "react-router-dom";

export default function RequireAuth({children}) {
    const {user} = useAuth();
    const location = useLocation();
    return !user ? <Navigate to="/login" replace state={{ path: location.pathname }}/> : children;

}