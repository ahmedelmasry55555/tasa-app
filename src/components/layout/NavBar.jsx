import {Avatar, Box, Flex, HStack, Wrap, WrapItem} from "@chakra-ui/react";
import {Link} from "react-router-dom";
import {useAuth} from "../../hooks/useAuth.jsx";

export default function NavBar() {

    const {user, logout} = useAuth();

    return (
        <Box bg="blackAlpha.800" w='100%' p={4} color='white' flex='true'>
            <Flex justify='space-between'>
                <Avatar name='AM' src='https://bit.ly/dan-abramov'/>
                <HStack spacing='24px'>
                    <Link to='/' exact="true">Home</Link>
                    {!user && (<Link to='/login'>Login</Link>)}
                    {user && (<Link to='/dashboard'>Dashboard</Link>)}
                    {user && (<button onClick={() => logout()}>Logout</button>)}

                </HStack>
            </Flex>
        </Box>
    )
}