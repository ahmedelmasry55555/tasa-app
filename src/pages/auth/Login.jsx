import {Formik, Field, useFormik} from "formik";
import {useNavigate} from "react-router-dom";

import {
    Box,
    Button,
    Checkbox,
    Flex,
    FormControl,
    FormLabel,
    FormErrorMessage,
    Input,
    VStack
} from "@chakra-ui/react";
import * as Yup from 'yup';
import axios from "axios";
import UseServerSideValidationErrors from "../../hooks/useServerSideValidationErrors.js";
import {useAuth} from "../../hooks/useAuth.jsx";


export default function Login() {
    let navigate = useNavigate();
    const {login} = useAuth();
    const {
        get: getServerSideValidationField,
        setErrors: SetServerSideValidationErrors
    } = UseServerSideValidationErrors([]);
    const formik = useFormik({
            initialValues: {
                email: "",
                password: "",
                rememberMe: false
            },
            validationSchema: Yup.object({
                email: Yup.string().email('Invalid email address').required(),
                password: Yup.string().min(5).required(),
            }),
            onSubmit: values => {
                axios.post('http://tasa-app.test/api/login', values).then((response) => {
                    return login(response.data.data);

                }).catch(errors => {
                    SetServerSideValidationErrors(errors.response.data.errors);
                });

            },
        }
    );
    return (
        <Flex bg="gray.100" align="center" justify="center" h="100vh">
            <Box bg="white" p={6} rounded="md" width="20%">
                <form onSubmit={formik.handleSubmit}>
                    <VStack spacing={4} align="flex-start">
                        <FormControl isInvalid={!!formik.errors.email && formik.touched.email}>
                            <FormLabel htmlFor="email">Email Address</FormLabel>
                            <Input
                                id="email"
                                name="email"
                                type="text"
                                variant="filled"
                                {...formik.getFieldProps('email')}
                            />
                            <FormErrorMessage>{formik.errors.email}</FormErrorMessage>
                        </FormControl>
                        <FormControl isInvalid={!!formik.errors.password && formik.touched.password}>
                            <FormLabel htmlFor="password">Password</FormLabel>
                            <Input

                                id="password"
                                name="password"
                                type="password"
                                variant="filled"
                                {...formik.getFieldProps('password')}
                            />
                            <FormErrorMessage>{formik.errors.password}</FormErrorMessage>
                            {/*<FormErrorMessage>{get('credentials')}</FormErrorMessage>*/}
                            <p>{getServerSideValidationField('credentials')}</p>
                        </FormControl>
                        <Checkbox
                            id="rememberMe"
                            name="rememberMe"
                            colorScheme="purple"
                            {...formik.getFieldProps('rememberMe')}
                        >
                            Remember me?
                        </Checkbox>

                        <Button type="submit" colorScheme="purple" isFullWidth>
                            Login
                        </Button>
                    </VStack>
                </form>

            </Box>
        </Flex>
    );
}