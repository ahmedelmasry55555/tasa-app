import {useAuth} from "../hooks/useAuth.jsx";

export default function Dashboard() {
    const {user} = useAuth();
    console.log(user,'dashboard')
    return (
        <>
            {user.name}
            <br/>
            {user.email}
        </>
    );
}